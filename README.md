# PoFace: Fuzz Face™ Fuzzbox (Guitar "Stomp-Box")
Copyright © 2019 Bart Massey

This circuit is an interpretation of the ubiquitous
[Fuzz Face][wp] fuzzbox. My version incorporates a bunch of
mods from the internet, producing a more-expensive
higher-parts-count circuit that should be a bit more
versatile and produce a nice effect.

This circuit is a work-in-progress: please excuse the mess.

## Circuit

<img src="poface.png" width=1170>

The Arbiter Fuzz Face is one of the most widely used guitar
fuzzboxes, counting all the variants made by Arbiter and
others down through the years. It was most famously used by
Jimi Hendrix. [MusicRadar][history] has a nice history, and
the [Wikipedia][wp] page has some good information.

There's *a lot* of discussion of the electronics of the
Fuzz Face and its siblings, successors, and variants on the
web. One of my starting points was slides from a Dorkbot PDX
Fuzz Face building [workshop][dorkbot]: those folks are local
and know what they are doing. I also relied heavily on
circuit blocks and detailed circuit analysis from
[Electrosmash][esmash], [General Guitar Gadgets][ggg],
[Coda Effects][coda], and [Fuzz Central][fcentral]. Many of
these contain complete solutions; while I decided to "do my
own thing" I definitely looked carefully at all of them. I
most closely followed R.G. Keen's [analysis][keen], which I
found to be well-written and insightful. Finally, Jack
Orman's analysis of the Fuzz Face [power circuit][muzique]
gave critical information on how to build a power circuit
that would handle modern batteries and power supplies.

It turns out there are a lot of decisions to be made when
building a Fuzz Face-style fuzzbox circuit. Let's work
through the circuit shown above and talk about why I
designed it the way I did. I also want to talk about some
standard stomp-box things along the way.

### NPN Transistors

The original Fuzz Face and many of the clones use germanium
PNP transistors. This was likely not a design decision so
much as an availability one at the time. The germanium PNP
parts can contribute to a unique sound. However, there are a
number of problems with them:

* Germanium transistors have a lot of base-collector
  capacitance. This is arguably a feature rather than
  a bug: it changes the sound.

* Germanium is quite temperature-sensitive. A temperature
  adjustment pot needs to be on the unit, and it needs to be
  adjusted each time the unit is brought into a warm or cold
  environment.

* Germanium transistors have quite a bit of variance in gain
  (β) and leakage current. It is pretty much required to buy
  a bunch of them and measure them to find ones with the
  parameters you want.

* PNP transistors really want to be driven from negative to
  positive. This means that the case ground cannot be the
  same as the signal ground, which in turn means that the
  standard positive-sleeve DC power plug (discussed below)
  isn't a good solution for supplying outside power. Most
  PNP Fuzz Faces are thus battery-only.

I chose to go with modern NPN transistors for my
circuit. This is not at all uncommon in Fuzz Face clones.
To try to achieve a sound closer to the original, I adjusted
resistor values along the lines of a typical NPN Fuzz Face
clone and added small bypass capacitors (see the box labeled
"NPN" in the schematic). I also added the "Fuller Mods" (see
below), which include a bias-point adjustment useful for
tuning up the NPN stage.

### Fuller Mods

Mike Fuller posted some recommendations to the net about
tuning up the Fuzz Face, as [reported][keen] by Keen.  I
have incorporated Fuller's two mods (see the boxes labeled
"FULLER" in the schematic).

* One Fuller mod allows tuning the operating bias point of
  the second transistor stage with a variable resistor
  (labeled "DEPTH" in the schematic).  This is crucial for
  germanium PNP transistors, but is also nice for NPN
  silicon to get a "better" sound.

* The second Fuller mod allows tuning the input impedance at
  the expense of gain with a variable resistor (labeled "INP"
  in the schematic). Because the input impedance of the base
  circuit is so low, it loads a guitar pickup (or other
  low-impedance source) heavily, which may not be
  desirable. By turning the input impedance up and turning
  the output volume up to compensate, the circuit becomes
  more independent of its surrounding.

### Supply Voltage

The original Fuzz Face uses PNP transistors and zinc-carbon
9V batteries. The circuit was tuned for this combination,
and really sounds best with this battery chemistry. Indeed,
many feel that a slightly spent battery sounds better than a
fully-charged one. Attempts to use "true" 9V alkaline
batteries or 9V DC supplies just don't sound
right. Fortunately, Orman has done a bunch of
[experimentation][muzique] and suggests a circuit
modification to make the circuit work in these situations
(see the box labeled "VOLTAGE SAG" in the schematic). A
zinc-carbon battery should not be used with this circuit, as
the voltage delivered to the amplifier will be lower than
desirable, but more modern power supplies should work
great. I used the power indicator LED as a voltage drop to
save a part: changing the LED color will change the voltage
drop, so choose accordingly. The large filter capacitor in
this modification is my addition, and is intended to reduce
supply noise from DC supplies.

### Component Values

There are some component choices here that are a bit
tricky. I have resorted to some guesswork: hopefully it
won't foul things up too much.

* The output decoupling capacitor is currently at
  0.047µF. This is a compromise between various values seen
  in schematics, and is actually rather large: 0.01µF is
  more common. This thing acts as something of an output
  high-pass filter, so I would think that larger would be
  better, but apparently not.

* The choice of 47pF for the NPN bypass capacitors was
  driven by looking at some data sheets for germanium PNP
  transistors.

### Power Supply

The power supply for this circuit is wired in a way that is
standard for stomp boxes. The 9V DC jack is tip-negative,
sleeve positive. This allows the optional 9V supply to cut
the battery out of the circuit when plugged in. The stereo
1/4" audio input jack uses ring and sleeve as spare
conductors: no current can flow through the circuit unless
the ring is shorted to the sleeve by inserting the audio
input cable plug. Finally, a pole from the TPDT
stomp switch is used to prevent power from flowing unless
the unit it turned on.

The net effect of all this is that the box consumes battery
power only when the stomp switch is in the effect position,
the audio input is plugged in, and the external DC input is
not plugged in. Together with the power indicator LED, this
minimizes the chance of needlessly consuming the battery.

### Stomp Switch

No modern stomp box would be without a stomp switch to
provide a bypass capability. As noted above, this switch is
also used as a power switch. The passive bypass wiring shown
here completely removes the circuit when in the bypass
("off") position.

There is some talk in the literature about "anti-pop"
capacitors, but I haven't understood the details. Perhaps I
will need to rework the wiring of this switch, but it should
be good enough to start.

## Working With This Circuit

You will need [KiCad][kicad] version 7.0 or better. I am
currently using Debian Linux KiCad 7.0.2+dfsg-1. The
`poface.pro` file should just work.

## Name

I chose "PoFace" because I've long gone by the handle
[PO8][po8] online. The term "[po-faced][pofaced]" is British
slang for a "solemn, serious, or earnest expression or
manner", "piously or hypocritically solemn".  I thought this
was a nice sarcastic commentary on my take on the simple
"build-a-fuzzbox" plan, and a homage to its London origins
as the Arbiter Fuzz Face.

## License

This work is licensed under the GPL version 3 or
later. Please see the file `LICENSE` in this distribution
for license terms.

[history]:  http://www.musicradar.com/news/the-history-of-the-fuzz-face
[wp]:       http://en.wikipedia.org/wiki/Fuzz_Face
[dorkbot]:  http://dorkbotpdx.org/sites/default/files/2016-04/FuzzFaceWorkshop.pdf
[esmash]:   http://www.electrosmash.com/fuzz-face
[ggg]:      http://www.generalguitargadgets.com/effects-projects/fuzz-tones/fuzz-faces/
[coda]:     http://www.coda-effects.com/p/circuit-analysis-fuzz-face.html
[fcentral]: http://fuzzcentral.ssguitar.com/fuzzface.php
[keen]:     http://www.geofex.com/article_folders/fuzzface/fffram.htm
[muzique]:  http://www.muzique.com/lab/batteryz.htm
[kicad]:    http://kicad-pcb.org
[po8]:      http://fob.po8.org/node/436
[pofaced]:  http://www.merriam-webster.com/dictionary/po-faced
